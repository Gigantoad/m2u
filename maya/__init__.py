""" maya module init file, imports the whole interface module, nothing else """

# for the sake of having a file with a meaningful name,
# all actual stuff is in mayaInterface.py instead of this __init__.py
from mayaInterface import *


